'use strict';
if (!window.console) window.console = {};
if (!window.console.memory) window.console.memory = function() {};
if (!window.console.debug) window.console.debug = function() {};
if (!window.console.error) window.console.error = function() {};
if (!window.console.info) window.console.info = function() {};
if (!window.console.log) window.console.log = function() {};

// sticky footer
//-----------------------------------------------------------------------------
if (!Modernizr.flexbox) {
    (function() {
        var
            $pageWrapper = $('#page-wrapper'),
            $pageBody = $('#page-body'),
            noFlexboxStickyFooter = function() {
                $pageBody.height('auto');
                if ($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
                    $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
                } else {
                    $pageWrapper.height('auto');
                }
            };
        $(window).on('load resize', noFlexboxStickyFooter);
    })();
}
if (ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
    (function() {
        var
            $pageWrapper = $('#page-wrapper'),
            $pageBody = $('#page-body'),
            ieFlexboxFix = function() {
                if ($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
                    $pageWrapper.height($(window).height());
                    $pageBody.removeClass('flex-none');
                } else {
                    $pageWrapper.height('auto');
                }
            };
        ieFlexboxFix();
        $(window).on('load resize', ieFlexboxFix);
    })();
}

$(function() {

    // placeholder
    //-----------------------------------------------------------------------------
    $('input[placeholder], textarea[placeholder]').placeholder();

});

//*****************//
//Load
//*****************//


$('.time').timepicker({
    show2400: true,
    scrollDefault: 'now',
    step: 60,
    'timeFormat': 'H:i'
});

$('.calendar').datetator({
    height: 'auto',
    useDimmer: true,
    labels: { // contains all the labels for the plugin, 
        // - this can be changed to other languages
        week: 'Н',
        dayNames: [
            'Пн',
            'Вт',
            'Ср',
            'Чт',
            'Пт',
            'Сб',
            'Вс'
        ],
        monthNames: [
            'Январь',
            'Февраль',
            'Март',
            'Апрель',
            'Май',
            'Июнь',
            'Июль',
            'Август',
            'Сентябрь',
            'Октябрь',
            'Ноябрь',
            'Декабрь'
        ],
        previousMonth: '«',
        nextMonth: '»',
        previousYear: '«',
        nextYear: '»',
        empty: 'Удалить',
        today: 'Сегодня',
        previousMonthTooltip: 'Прошлый месяц',
        nextMonthTooltip: 'Следующий месяц',
        previousYearTooltip: 'Прошлый год',
        nextYearTooltip: 'Следующий год',
        emptyTooltip: 'Удалить дату',
        // todayTooltip: 'Show and choose today\'s date'
    }
});

//*****************//
//Go bottom
//*****************//


$('.js-go-bottom').on('click', function(e) {
    var height = $('.food-wrap').offset().top;
    e.preventDefault();
    $("body, html").animate({
        scrollTop: height + 72
    }, 800);
})


//*****************//
//Slider Head Options//
//*****************//

$('.gallery').slick({
    arrows: false,
    dots: true,
});

$('.slider-rest').slick({
    arrows: false,
    dots: true,
});

$('.partn-slider').slick({
    slidesToShow: 4,
    dots: false,
    arrows: false,
    responsive: [{
        breakpoint: 640,
        settings: {
            slidesToShow: 3,
        }
    }]
})


//*****************//
//Google Map//
//*****************//


google.maps.event.addDomListener(window, "load", initMap);

function initMap() {
    var myLatlng = new google.maps.LatLng(55.803185, 37.641228);
    var map = new google.maps.Map(document.getElementById('map'), {
        center: new google.maps.LatLng(55.803185, 37.641228),
        zoom: 17,
        scrollwheel: false,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        streetViewControl: false,
        rotateControl: false,
        draggable: false,
    });
    var marker = new google.maps.Marker({
        map: map,
        position: myLatlng,
    });
};

//*****************//
//Slider
//*****************//

$('.menu-list ul').flickity({
    freeScroll: true,
    contain: true,
    prevNextButtons: false,
    pageDots: false,
});


$(window).load(function() {
    var lenght = 0;
    $('.menu-list ul li').each(function() {
        lenght = lenght + $(this).outerWidth();
    });
    if ($(window).outerWidth() < (lenght + 100)) {
        $('.menu-list ul').flickity({
            freeScroll: true,
            contain: true,
            prevNextButtons: false,
            pageDots: false,
        });

    } else {
        $('.menu-list ul').flickity({}).flickity('destroy');
    }
});

$(window).resize(function() {
    var lenght = 0;
    $('.menu-list ul li').each(function() {
        lenght = lenght + $(this).outerWidth();
    });
    if ($(window).outerWidth() < (lenght + 100)) {
        $('.menu-list ul').flickity({
            freeScroll: true,
            contain: true,
            prevNextButtons: false,
            pageDots: false,
        });

    } else {
        $('.menu-list ul').flickity({}).flickity('destroy');
    }
});

//*****************//
//Ancher Scroll
//*****************//

$('.menu-list ul li a').on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 60
    }, 800);
    return false;
})

$('.js-show-menu').on('click', function(e) {
    e.preventDefault();
    var pos = $(window).scrollTop();
    var winHeight = $(window).outerHeight();


    if ($(this).hasClass('active')) {
        var _this = this;
        $('.menu').fadeOut(300);
        $('.menu').removeClass('active');
        $('.page-wrapper').removeClass('rotate-cls');
        $('body').removeClass('rotate-effect');
        $('.header').fadeIn(800);
        $(_this).removeClass('active');

    } else {
        $('.page-wrapper').css({
            'transform-origin': '100%' + ((winHeight / 2) + pos) + 'px'
        });
        setTimeout(function() {
            $('.page-wrapper').addClass('rotate-cls');
        }, 500)
        $('body').addClass('rotate-effect');
        //$('.menu').delay(400).fadeIn('fast').addClass('active');

        $('.menu').delay(200).fadeIn(300);
        setTimeout(function() {
            $('.menu').addClass('active');
        }, 450)


        if (pos > 100) {
            $('.header').fadeOut('fast');
        }
        $('.menu-btn').addClass('active');
    }
});

//*****************//
//Validate
//*****************//

$('#datetator_').attr('name', 'calendar');

$('#booking-form').validate({
    rules: {
        name: {
            required: true
        },
        phone: {
            required: true,
            digits: true
        },
        calendar: {
            required: true
        },
        time: {
            required: true
        },

    },
    messages: {
        name: {
            required: "Введите коректное имя"
        },
        phone: {
            required: "Введите коректный номер телефона",
            digits: "Введите коректный номер телефона"
        },
        calendar: {
            required: "Введите дату"
        },
        time: {
            required: "Введите время"
        },
    }
});

//*****************//
//Date & Time input plugins
//*****************//

$('.ui-timepicker-input').on('click', function() {
    $('.ui-timepicker-wrapper').css('width', $('input.time').outerWidth() - 2);
});

$('input.datetator').on('click', function() {
    $('.datetator_picker').css('width', $('input.calendar').outerWidth()-2);
});


//*****************//
//Final Window load
//*****************//


$(window).load(function() {
    $('.page-wrapper').fadeTo('fast', 1);
    $('.menu-btn').delay(200).fadeTo('fast', 1);
    setHeight()
})

function  setHeight(){
    var windowHeight = $(window).outerHeight() < 350 ? 550 : $(window).outerHeight();
    var windowWidth = $(window).width();
    if (windowWidth >= 320 && windowHeight > 300) {
        $('.gallery').css('height', windowHeight);
    }
}